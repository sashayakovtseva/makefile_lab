default:libs
	gcc -L./target -Wall -o target/main src/main.c -lhello -lbye

libs: clean
	mkdir target
	gcc -c -Wall -Werror -fpic src/hellolib.c -o target/hellolib.o
	gcc -shared -o target/libhello.so target/hellolib.o

	gcc -c -Wall -Werror -fpic src/byelib.c -o target/byelib.o
	gcc -shared -o target/libbye.so target/byelib.o

clean:
	rm -rf target
